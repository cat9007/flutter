import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      routes: {
        '/home': (context) => Home(),
        '/a': (context) => A(),
        '/b': (context) => B(),
        '/c': (context) => C(),
      },
      initialRoute: '/',
      home: Home(),
      onGenerateRoute: (RouteSettings settings) {
        if (settings.name == '/z') {
          return MaterialPageRoute(builder: (context) => C());
        }
      },
      onUnknownRoute: (RouteSettings settings) {
        return MaterialPageRoute(builder: (content) => U());
      },
    );
  }
}

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('home')),
      body: Center(
        child: ElevatedButton.icon(
            onPressed: () {
              Navigator.pushNamed(context, '/404', arguments: 'tangguodalong');
            },
            icon: Icon(Icons.home),
            label: Text('home')),
      ),
    );
  }
}

class A extends StatefulWidget {
  A({Key? key}) : super(key: key);

  @override
  State<A> createState() => _AState();
}

class _AState extends State<A> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('A')),
      body: Center(
        child: ElevatedButton.icon(
            onPressed: () {
              Navigator.pop(context, 'tina');
            },
            icon: Icon(Icons.home),
            label: Text('A ')),
      ),
    );
  }
}

class B extends StatefulWidget {
  const B({Key? key}) : super(key: key);

  @override
  State<B> createState() => _BState();
}

class _BState extends State<B> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('B')),
      body: Center(
        child: ElevatedButton.icon(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return C();
              }));
            },
            icon: Icon(Icons.home),
            label: Text('B')),
      ),
    );
  }
}

class C extends StatefulWidget {
  const C({Key? key}) : super(key: key);

  @override
  State<C> createState() => _CState();
}

class _CState extends State<C> {
  @override
  Widget build(BuildContext context) {
    var params = ModalRoute.of(context)?.settings.arguments;
    return Scaffold(
      appBar: AppBar(title: Text('C')),
      body: Center(
        child: ElevatedButton.icon(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.home),
            label: Text('C ${params}')),
      ),
    );
  }
}

class U extends StatefulWidget {
  U({Key? key}) : super(key: key);

  @override
  State<U> createState() => _UState();
}

class _UState extends State<U> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('C')),
      body: Center(
        child: Text(
          '404',
          textScaleFactor: 5,
        ),
      ),
    );
  }
}
