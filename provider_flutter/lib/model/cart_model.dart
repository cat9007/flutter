import 'package:flutter/material.dart';
import 'package:provider_flutter/page/catalog.dart';

class CartModel extends ChangeNotifier {
  List<Product> _cart = <Product>[];
  List<Product> get cart => _cart;

  void add(Product item) {
    this._cart.add(item);
    notifyListeners();
  }

  void remove(Product item) {
    this._cart.remove(item);
    notifyListeners();
  }

  void removeAll() {
    this._cart.clear();
    notifyListeners();
  }
}
